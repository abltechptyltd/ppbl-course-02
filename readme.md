# Plutus Project-Based Learning Summer 2022

## Welcome!
This repository consists of a series of a Plutus starter projects. Each project is in a dedicated subdirectory, ie `/project-01`.

## Current List of Projects:
- `/project-01`: The "Always Succeeds" contract, can be used as a starting template for substantive Plutus projects.

## Full Plutus PBL Course on Canvas
[Link coming soon!]()